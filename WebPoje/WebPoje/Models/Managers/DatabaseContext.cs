﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebPoje.Models.Managers
{
    public class DatabaseContext:DbContext
    {
        public DbSet<Kisiler> Kisiler { get; set; }
        public DbSet<Urunler> Urunler { get; set; }
        public DbSet<Kategoriler> Kategoriler { get; set; }

    }
}