﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebPoje.Models
{
    [Table("Kisiler")]
    public class Kisiler
    {
        [Required,
             DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required,
            DisplayName("Kullanıcı Adı"),
            MinLength(5, ErrorMessage = "{0} min. {1} karakter olmalıdır."),
            MaxLength(20, ErrorMessage = "{0} max. {1} karakter olmalıdır.")]
        public string KullanıcıAdı { get; set; }
        [DisplayName("Adı"),
            MinLength(5, ErrorMessage = "{0} min. {1} karakter olmalıdır."),
            MaxLength(20, ErrorMessage = "{0} max. {1} karakter olmalıdır.")]
        public string Ad { get; set; }
        [DisplayName("Soyadı"),
            Required(ErrorMessage = "Lütfen {0} degerini boş bırakmayın!"),
            MinLength(5, ErrorMessage = "{0} min. {1} karakter olmalıdır."),
            MaxLength(20, ErrorMessage = "{0} max. {1} karakter olmalıdır.")]
        public string Soyad { get; set; }
        [DisplayName("Eposta"),
            Required(ErrorMessage = "Lütfen {0} degerini boş bırakmayın!"),
            MaxLength(60, ErrorMessage = "{0} adresi max. {1} karakter olmalıdır."),
            EmailAddress(ErrorMessage = "Lütfen {0} Adresi Giriniz!")]
        public string Email { get; set; }
        [DisplayName("Şifre"),
            Required(ErrorMessage = "Lütfen {0} degerini boş bırakmayın!"),
            MinLength(8, ErrorMessage = "{0} adresi min. {1} karakter olmalıdır."),
            MaxLength(16, ErrorMessage = "{0} adresi max. {1} karakter olmalıdır."),
            DataType(DataType.Password, ErrorMessage = "Lütfen {0} degerini doğru giriniz!")]
        public string Sifre { get; set; }
        [DisplayName("Şifre(Tekrar)"),
            Required(ErrorMessage = "Lütfen {0} degerini boş bırakmayın!"),
            MinLength(8, ErrorMessage = "{0} adresi min. {1} karakter olmalıdır."),
            MaxLength(16, ErrorMessage = "{0} adresi max. {1} karakter olmalıdır."),
            DataType(DataType.Password, ErrorMessage = "Lütfen {0} degerini doğru giriniz!"),
            Compare(nameof(Sifre), ErrorMessage = "Şifreler Birbiriyle Uyuşmuyor !")]
        public string SifreTekrar { get; set; }
    }
}