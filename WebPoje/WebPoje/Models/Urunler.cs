﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebPoje.Models
{
    [Table("Urunler")]
    public class Urunler
    {
        [Key(),DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UrunId { get; set; }
        [Required]
        public String UrunAdı { get; set; }
        [Required]
        public int UrunUcreti { get; set; }
        
        public string UrunResmi { get; set; }
    }
}