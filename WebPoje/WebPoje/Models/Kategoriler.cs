﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebPoje.Models
{
    [Table("Kategoriler")]
    public class Kategoriler
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int KategoriId { get; set; }
        [Required]
        public string KategoriAdi { get; set; }
    }
}