﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPoje.Models;
using WebPoje.Models.Managers;

namespace WebPoje.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            DatabaseContext db = new DatabaseContext();
            db.Kisiler.ToList();
            return View();
        }

        public ActionResult Urun()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string KullanıcıAdı, string Sifre)
        {
            Kisiler uye = null;
            if (KullanıcıAdı != null && Sifre != null)
            {
                DatabaseContext db = new DatabaseContext();
                uye = db.Kisiler.Where(x => x.KullanıcıAdı == KullanıcıAdı && x.Sifre == Sifre).FirstOrDefault();
                ViewBag.Mesaj = null;
                if (uye != null)
                {
                    if (uye.KullanıcıAdı == "admin")
                    {
                        return RedirectToAction("Index", "Yonetim");
                    }
                    Session["KullanıcıAdı"] = uye.KullanıcıAdı;
                    Session["Adı"] = uye.Ad;
                    Session["Soyadı"] = uye.Soyad;
                    Session["Email"] = uye.Email;
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Mesaj = "Malesef Böyle Bir Üye Bulunamadı!!! Kullanıcı Adı ve Şifreyi Kontrol Edip Tekrar Deneyin.";
                    ViewBag.Alert = "danger";
                    return View();
                }
            }
            return View();
        }

        public ActionResult Sign()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Sign(Kisiler uye)
        {
            if (ModelState.IsValid)
            {
                DatabaseContext db = new DatabaseContext();
                db.Kisiler.Add(uye);
                if (db.SaveChanges() > 0)
                {
                    ViewBag.Mesaj = "Başarıyla Üye Oldunuz...";
                    ViewBag.Alert = "success";
                }
                else
                {
                    ViewBag.Mesaj = "Üye Olunurken Beklenmeyen Bir Hata Oluştu!";
                    ViewBag.Alert = "danger";
                }
                //return RedirectToAction("Mesaj");
            }
            return View(uye);
        }

        public ActionResult CikisYap()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}